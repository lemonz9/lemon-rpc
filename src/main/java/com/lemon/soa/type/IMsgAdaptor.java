package com.lemon.soa.type;

/**
 * The Adaptor Type.
 * 
 * me
 */
public interface IMsgAdaptor {

    /**
     * Getter filter msg.
     * 
     * @return
     */
    String getMsg();

    /**
     * Setter filter msg.
     * 
     * @param msg
     */
    void setMsg(String msg);

}
