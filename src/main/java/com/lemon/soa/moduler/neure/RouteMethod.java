package com.lemon.soa.moduler.neure;

/**
 * 路由方法
 * 
 * me
 */
public enum RouteMethod {

    /**
     * 路由服务
     */
    ROUTE,

    /**
     * MOCK服务
     */
    MOCK,

    /**
     * 呼吸服务
     */
    BREATHCYCLE,

    /**
     * 回调服务
     */
    CALLBACK,

    /**
     * 失败告警服务
     */
    ALARM;

}
