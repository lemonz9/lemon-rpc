package com.lemon.soa.moduler.extension.slow;

import com.lemon.soa.moduler.senior.alarm.processor.IAlarmTypeProcessor;

public interface ISlowProcessor<REQ, RES> extends IAlarmTypeProcessor<REQ, RES> {

}
