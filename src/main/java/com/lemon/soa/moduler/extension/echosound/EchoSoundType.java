package com.lemon.soa.moduler.extension.echosound;

public enum EchoSoundType {

    /**
     * 回声探测请求
     */
    REQ,

    /**
     * 回声探测响应
     */
    RES,

    /**
     * 非回声探测
     */
    NON;

}
