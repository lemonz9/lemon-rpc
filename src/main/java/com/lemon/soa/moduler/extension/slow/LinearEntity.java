package com.lemon.soa.moduler.extension.slow;

/**
 * 线性函数参数对象
 * 
 * me
 */
public class LinearEntity {

    private double a;
    private double b;

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

}