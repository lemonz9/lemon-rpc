package com.lemon.soa;

/**
 * 通知
 * 
 * me
 */
public interface INotify<MSG> {

    void notify(MSG msg);

}
