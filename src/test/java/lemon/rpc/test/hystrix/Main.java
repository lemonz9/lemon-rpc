package lemon.rpc.test.hystrix;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import rx.Observable;
import rx.Observer;
import rx.functions.Action1;

public class Main {

    public static void main(String[] args) {
        //        String s = new CommandHelloWorld("World").execute();
        //        System.out.println(s);

        try {
            Future<String> fs = new CommandHelloWorld("World").queue();
            System.out.println(fs.get());

            Observable<String> ho = new CommandHelloWorld("World").observe();
            ho.subscribe(new Action1<String>() {
                @Override
                public void call(String s) {
                }
            });

            Observable<String> fWorld = new CommandHelloWorld("World").observe();

            //            // blocking
            //            assertEquals("Hello World!", fWorld.toBlockingObservable().single());
            //            assertEquals("Hello Bob!", fBob.toBlockingObservable().single());

            // non-blocking 
            // - this is a verbose anonymous inner-class approach and doesn't do assertions
            fWorld.subscribe(new Observer<String>() {

                @Override
                public void onCompleted() {
                    // nothing needed here
                }

                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                }

                @Override
                public void onNext(String v) {
                    System.out.println("onNext: " + v);
                }

            });

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
